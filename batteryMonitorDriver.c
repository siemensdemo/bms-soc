/* This is the source file we would include with the model generated code
 * so that the the model code can automatically communicate with our 
 * "Battery Monitor" hardware to get sensor readings from the battery
 * "The Sensor Reading w/ Fault Detection" Stateflow Chart calls these 
 * functions
 *
 * These are stubbed versions of these functions for the purposes of this 
 * example */

int readTemperature() {
    return 1;
}

int readVoltage(){
    return 1;
}

int readCurrent() {
    return 1;
}